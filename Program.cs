﻿using System;
using System.Collections;

namespace ex_queue
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create an application with a menu that uses a queue that will allow the user to:
            // 1.Add items
            // 2.Delete items
            // 3.SHow the number of items in the queue
            // 4.Find an item
            // 5.Display all of the items in the queue
            // 6.Exit
            //

            Queue que = new Queue();
            int menuNum;

            do
            {
                Console.WriteLine("-------------------------------------------------");
                Console.WriteLine("1. Add items");
                Console.WriteLine("2. Delete items");
                Console.WriteLine("3. SHow the number of items in the queue");
                Console.WriteLine("4. Find an item");
                Console.WriteLine("5. Display all of the items in the queue");
                Console.WriteLine();
                Console.WriteLine("6.Exit");
                Console.WriteLine("-------------------------------------------------");

                Console.Write("Choose one and press enter key : ");
                Int32.TryParse(Console.ReadLine(), out menuNum);

                switch(menuNum)
                {
                    case 1 : add(que); break;
                    case 2 : delete(que); break;
                    case 3 : countItems(que); break;
                    case 4 : findItem(que); break;
                    case 5 : displayItem(que); break;
                    case 6 : exit(que); break;
                    default : break;
                }

                Console.ReadLine();
                Console.Clear();

            } while(menuNum != 6);
            
        }

        private static void add(Queue q)
        {
            Console.Write("Type in any words to add into a queue, then press enter key : ");
            q.Enqueue(Console.ReadLine());
        }
        
        private static void delete(Queue q)
        {
            Console.WriteLine("Dequeued a item.");
            q.Dequeue();
        }

        private static void countItems(Queue q)
        {
            Console.WriteLine("The number of items is " + q.Count);
        }

        private static void findItem(Queue q)
        {
            Console.WriteLine("What item would you find.");
            Console.Write("Type in the name or value of item : ");
            
            string itemValue = Console.ReadLine();
            bool isExist = q.Contains(itemValue);

            if(isExist)
            {
                Console.WriteLine("The item '"+itemValue+"' is contained in the queue.");
            }
            else
            {
                Console.WriteLine("Sorry, we cannot find the item '"+itemValue+"' in the queue.");
            }
        }

        private static void displayItem(Queue q)
        {
            foreach(string item in q)
            {
                Console.WriteLine(item);
            }
        }

        private static void exit(Queue q)
        {
            Console.WriteLine("Thank you!");
            q.Clear();
        }
    }
}
